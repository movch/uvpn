require 'droplet_kit'

puts 'Initializing DO client...'
client = DropletKit::Client.new(access_token: ENV['DO_API_KEY'])

puts 'Cleaning up droplets...'
client.droplets.delete_for_tag(tag_name: 'sshuttle')