# uVPN

A tiny Ruby script that sets up a virtual machine on Digital Ocean and then creates VPN over SSH using [sshuttle](https://github.com/sshuttle/sshuttle) utility.

## Installation and usage

Log in to your Digital Ocean account ([create and get 10$](https://m.do.co/c/9bb5125d6e8f) if you don't have one), [add your SSH key](https://cloud.digitalocean.com/settings/security) to it, and [generate API key](https://cloud.digitalocean.com/settings/api/tokens).

Install sshuttle on your client machine.

    brew install sshuttle

If you don't have Bundler run:

    sudo gem install bundler

Navigate to project directory and install dependencies for the script ([DO API wrapper](https://github.com/digitalocean/droplet_kit)):

    bundle install --path vendor/cache

Run script (don't forget to put your API key to environment variable DO_API_KEY):

    export DO_API_KEY='be0a....' ; bundle exec ruby vpn.rb ; bundle exec ruby remove_droplets.rb

Follow onscreen instructions (sshuttle needs sudo) and wait for `client: Connected.` message, then you can start use your vpn. Check your IP with command:

    curl ipinfo.io/ip

Make sure it the same as the IP of your droplet (`Obtained droplet IP:` message).

When you're done you can just press `Ctrl + C` and the droplet should be removed automatically.