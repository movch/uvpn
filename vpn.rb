require 'droplet_kit'

puts 'Initializing DO client...'
client = DropletKit::Client.new(access_token: ENV['DO_API_KEY'])

puts 'Collecting SSH keys...'
my_ssh_keys = client.ssh_keys.all.collect {|key| key.fingerprint}

puts 'Selecting random region...'
region = client.regions.all.collect{ |region| region.slug }.sample

puts "Trying to create droplet in #{region.upcase} region..."
droplet = DropletKit::Droplet.new(name: 'vpn', region: region, image: 'ubuntu-16-04-x64', size: '512mb', ssh_keys: my_ssh_keys, tags: ['sshuttle'])
created = client.droplets.create(droplet)

puts 'Waiting for droplet'
droplet_status = client.droplets.find(id: created.id.to_s).status
while droplet_status != 'active' do
  print('.')
  sleep(5)
  droplet_status = client.droplets.find(id: created.id.to_s).status
end

droplet_ip = client.droplets.find(id: created.id.to_s).public_ip
puts "\nObtained droplet IP: #{droplet_ip}"

puts 'Waiting for SSH...'
ssh_check_cmd = "ssh -o BatchMode=yes -o ConnectTimeout=3 -o StrictHostKeyChecking=no root@#{droplet_ip} echo ok 2>&1"
ssh_status = `#{ssh_check_cmd}`
while ssh_status.chomp != 'ok' do
  print('.')
  sleep(5)
  ssh_status = `#{ssh_check_cmd}`
end

puts "\nTrying to connect via sshuttle..."
exec "sshuttle --dns -r root@#{droplet_ip} -x #{droplet_ip} 0/0 || true"